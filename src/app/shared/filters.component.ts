import { Component, EventEmitter, inject, Input, OnInit, Output, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { debounceTime } from 'rxjs';
import { FiltersActions } from '../core/store/filters/filters.actions';
import { FiltersState } from '../core/store/filters/filters.feature';
import { ProductsActions } from '../core/store/products/products.actions';
import { WeatherComponent } from './weather.component';

@Component({
  selector: 'app-filters',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  template: `
    
    <form [formGroup]="form">
      @for(field of fields; track $index) {
        @if (field === 'text') {
          <input type="text" formControlName="text" placeholder="text">
        }

        @if (field === 'gender') {
          <input type="text" formControlName="gender" placeholder="Gender">
        }
      }
    </form>
    
    
  `,
  styles: ``
})
export class FiltersComponent implements OnInit {
  @Input() filters: FiltersState | null = null;
  // @Input() field: string[] = []
  @Input() fields: (keyof FiltersState)[] = []
  @Output() changeFilters = new EventEmitter<Partial<FiltersState>>()

  fb = inject(FormBuilder)

  form!: FormGroup;

  view = inject(ViewContainerRef)

  ngOnInit() {

    /*
    const compo = this.view.createComponent(WeatherComponent)
    compo.setInput('city', 'milan')
    */

    const formCfg: Partial<FiltersState> = {}

    this.fields.forEach(field => {
      formCfg[field] = DICT[field]
      /*if (field === 'text') {
        formCfg.text = ''
      }
      if (field === 'gender') {
        formCfg.gender = 'M'
      }*/
    })

    this.form = this.fb.nonNullable.group<Partial<FiltersState>>(formCfg)
    /*this.form = this.fb.nonNullable.group<Partial<FiltersState>>({
      text: {'', Validators....},
      gender: 'M',

    })*/


  if (this.filters)
    this.form.patchValue(this.filters)

  this.form.valueChanges
    .pipe(
      debounceTime(1000)
    )
    .subscribe(text => {
      this.changeFilters.emit(this.form.value)
    })
  }
}

const DICT: any = {
  text: '',
  gender: 'M'
}
