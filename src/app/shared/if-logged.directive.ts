import {
  Directive,
  effect,
  ElementRef,
  HostBinding,
  inject,
  Input,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { AuthService } from '../core/auth.service';

@Directive({
  selector: '[appIfLogged]',
  standalone: true
})
export class IfLoggedDirective {
  @Input({ alias: 'appIfLogged'})
  requiredRole: 'admin' | 'mod' | undefined;

  @HostBinding('style.background') bg = 'yellow'

  tpl = inject(TemplateRef)
  view = inject(ViewContainerRef)
  authSrv = inject(AuthService)

  constructor() {

    effect(() => {

      if (this.authSrv.role() === this.requiredRole) {
        this.view.createEmbeddedView(this.tpl)
      } else {
        this.view.clear()
      }
    });

  }


}
