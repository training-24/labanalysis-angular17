import { Directive, ElementRef, HostBinding, HostListener, inject } from '@angular/core';

@Directive({
  selector: '[appHighlight]',
  standalone: true
})
export class HighlightDirective {
  /*@HostBinding() title = 'ciao'
  @HostBinding() class = 'highlight'

  @HostListener('click')
  clickMe() {
    window.alert('ciao')
  }*/

  el = inject(ElementRef)

  ngAfterViewInit() {
    this.el.nativeElement.style.fontSize = '50px'
    this.el.nativeElement.addEventListener('click', () => {
      window.alert('ciaone!')
    })
  }

}
