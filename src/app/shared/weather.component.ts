import { UpperCasePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, effect, inject, input, signal } from '@angular/core';

@Component({
  selector: 'app-weather',
  standalone: true,
  imports: [
    UpperCasePipe
  ],
  template: `
    <p>
      weather works! {{uppercase()}}
    </p>
    temperature: {{temperature()}}
    
    {{pippo}}
    
  `,
  styles: ``
})
export class WeatherComponent {
  pippo = 123;
  http = inject(HttpClient)
  city = input<string | undefined>('')
  uppercase = computed(() => this.city()?.toUpperCase())
  meteo = signal<any | null>(null);
  temperature = computed(() => this.meteo()?.main?.temp)

  constructor() {
    effect(() => {
      if (this.city()) {
        this.http.get<any>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city()}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
          .subscribe(res => {
            this.meteo.set(res)
          })
      }

    });
  }
}
