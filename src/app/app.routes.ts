import { Routes } from '@angular/router';
import { provideState } from '@ngrx/store';
import { counterFeature } from './core/store/counter/counter.feature';

export const routes: Routes = [
  { path: 'home', loadComponent: () => import('./features/home.component')},
  {
    path: 'shop',
    loadComponent: () => import('./features/shop.component')
  },
  { path: 'cart', loadComponent: () => import('./features/cart.component')},
  {
    path: 'demo-counter',
    loadComponent: () => import('./features/demo-counter.component'),
    providers: [
      provideState({
        name: counterFeature.name,
        reducer: counterFeature.reducer
      }),
    ]
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];
