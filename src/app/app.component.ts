import { JsonPipe } from '@angular/common';
import { Component, inject, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { AuthService } from './core/auth.service';
import { selectValue } from './core/store/counter/counter.feature';
import { HighlightDirective } from './shared/highlight.directive';
import { IfLoggedDirective } from './shared/if-logged.directive';
import { WeatherComponent } from './shared/weather.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, WeatherComponent, JsonPipe, HighlightDirective, IfLoggedDirective, RouterLinkActive],
  template: `
    <button routerLink="home">home</button>
    <button routerLink="shop">shop</button>
    <button routerLink="cart" routerLinkActive="active">cart</button>
    <button routerLink="demo-counter" appHighlight>demo-counter</button>
    
    <div city="Trieste"></div>
      
    <button *appIfLogged="'admin'" (click)="authSrv.logout()">Logout</button>
    
    {{authSrv.isLogged() | json}}
    {{authSrv.role()}}
    <hr>
    <router-outlet />
    
  `,
  styles: [],
})
export class AppComponent {
  authSrv = inject(AuthService)

  view = inject(ViewContainerRef)


}
