import { JsonPipe } from '@angular/common';
import { Component, computed, inject, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { combineLatest, debounce, debounceTime, startWith } from 'rxjs';
import { CartActions } from '../core/store/cart/cart.actions';
import { FiltersActions } from '../core/store/filters/filters.actions';
import { FiltersState, selectFilteredList, selectFiltersState } from '../core/store/filters/filters.feature';
import { ProductsActions } from '../core/store/products/products.actions';
import { selectList } from '../core/store/products/products.feature';
import { Product } from '../model/product';
import { FiltersComponent } from '../shared/filters.component';

@Component({
  selector: 'app-shop',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe,
    FiltersComponent
  ],
  template: `
    <h1>shop works!</h1>
    
    <app-filters
      [filters]="filters()"
      [fields]="['text', 'gender', 'text']"
      (changeFilters)="doSearch($event)" />
    
    @for(product of products(); track product.id) {
      <div>
        {{product.name}} - {{product.cost}}
        <button (click)="addToCart(product)">Add</button>
      </div>  
    } @empty {
      <div>no items</div>
    }
  `,
  styles: ``
})
export default class ShopComponent {
  store = inject(Store)
  products = this.store.selectSignal(selectFilteredList)
  filters = this.store.selectSignal(selectFiltersState)

  ngOnInit() {
    this.store.dispatch(ProductsActions.load())
  }

  doSearch(filters: Partial<FiltersState>) {
    this.store.dispatch(FiltersActions.search(filters));
  }


  addToCart(item: Product) {
    this.store.dispatch(CartActions.add({ item }))
  }

}
