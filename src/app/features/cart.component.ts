import { Component, inject } from '@angular/core';
import { AuthService } from '../core/auth.service';

@Component({
  selector: 'app-cart',
  standalone: true,
  imports: [],
  template: `
    <p>
      cart works!
    </p>
    
    <h1>Login Simulation</h1>
    
    <button (click)="authSrv.login()">login</button>
  `,
  styles: ``
})
export default class CartComponent {
  authSrv = inject(AuthService)
}
