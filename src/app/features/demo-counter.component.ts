import { AsyncPipe, JsonPipe, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, computed, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { CounterActions } from '../core/store/counter/counter.actions';
import { selectMultiplied, selectMultiplier, selectValue } from '../core/store/counter/counter.feature';

@Component({
  selector: 'app-demo-counter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    AsyncPipe,
    NgIf,
    JsonPipe,
    ReactiveFormsModule
  ],
  template: `
    <p>demo-counter works!</p>
    
    {{counter$ | async}} - {{ counter()}}
    
    <div>multiplied: {{multiplied()}}</div>
    <button (click)="dec()">-</button>
    <button (click)="inc()">+</button>
  `,
  styles: ``
})
export default class DemoCounterComponent {
  input = new FormControl()
  store = inject(Store)
  counter$ = this.store.select(selectValue)
  counter = this.store.selectSignal(selectValue)
  multiplied = this.store.selectSignal(selectMultiplied)

  text = toSignal(this.input.valueChanges);

  inc() {
    this.store.dispatch(CounterActions.increment())
  }
  dec() {
    this.store.dispatch(CounterActions.decrement({ value: 2}))
  }
/*

  constructor() {
    // ---o-o-o-o->
    // forkJoin({
    combineLatest({
      users: this.http.get<any[]>('https://jsonplaceholder.typicode.com/users'),
      posts: this.http.get<any[]>('https://jsonplaceholder.typicode.com/posts'),
      timer: interval(1000),
    })
      .subscribe(val => console.log(val))
  }
*/

}


