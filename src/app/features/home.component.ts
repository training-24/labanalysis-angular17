import { NgIf } from '@angular/common';
import { Component, computed, effect, ElementRef, signal, viewChild, ViewChild } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { debounceTime, startWith } from 'rxjs';
import { IfLoggedDirective } from '../shared/if-logged.directive';
import { WeatherComponent } from '../shared/weather.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    WeatherComponent,
    ReactiveFormsModule,
    NgIf,
    IfLoggedDirective
  ],
  template: `
    <p>
      home works! {{compoPIppo()}}
    </p>
    <input type="text" [formControl]="input" #ref >

   
      <app-weather [city]="value()" />  
   
    
    <div class="row">
      <div class="col-3"></div>
      <div class="col-3"></div>
      <div class="col-3"></div>
    </div>
   
  `,
  styles: ``
})
export default class HomeComponent {
   //@ViewChild('ref') input: ElementRef<HTMLInputElement> | undefined
  inputRef = viewChild<ElementRef<HTMLInputElement>>('ref')
  compo = viewChild(WeatherComponent)
  compoPIppo = computed(() => this.compo()?.pippo)

  input = new FormControl('Trieste', { nonNullable: true })

  value = toSignal( this.input.valueChanges
    .pipe(
      startWith(''),
      debounceTime(1000)
    ))

  ngOnInit() {
    this.compo()!.pippo = 456

  }
  constructor() {

      effect(() => {

      console.log('qui2', this.compo()?.pippo )
      this.inputRef()?.nativeElement.focus()
    });
  }

}
