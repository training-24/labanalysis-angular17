// model/product.t
export type Product = {
  id: number;
  name: string;
  cost: number;
}

