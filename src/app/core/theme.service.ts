import { computed, Injectable, signal } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  value = signal('dark')


  isDark = computed(() => this.value() === 'dark')
}
