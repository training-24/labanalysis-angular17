import { createFeature, createReducer, createSelector, on } from '@ngrx/store';
import { CounterActions } from './counter.actions';


export interface CounterState {
  value: number;
  multiplier: number;
}

const initialState: CounterState = {
  value: 0,
  multiplier: 10
}

export const counterFeature = createFeature({
  name: 'counter',
  reducer: createReducer(
    initialState,
    on(CounterActions.increment, (state, action) => ({ ...state, value: state.value + 1})),
    on(CounterActions.decrement, (state, action) => ({ ...state, value: state.value - action.value})),
  ),
  extraSelectors: ({ selectValue, selectMultiplier, selectCounterState }) => ({
    selectMultiplied: createSelector(
      selectValue,
      selectMultiplier,
      (value, multiplier) => value * multiplier
    ),
  })
})
export const {
  selectValue,
  selectMultiplier,
  selectCounterState,
  selectMultiplied,
} = counterFeature


