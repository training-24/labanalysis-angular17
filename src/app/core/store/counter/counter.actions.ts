import { createAction, createActionGroup, emptyProps, props } from '@ngrx/store';

/*
export const increment = createAction('[counter] increment')
export const decrement = createAction('[counter] increment', props<{ value: null}>())
export const reset = createAction('[counter] reset')
*/

export const CounterActions = createActionGroup({
  source: 'counter',
  events: {
    'Increment': emptyProps(),
    'Decrement': props<{ value: number}>(),
    'Reset': emptyProps()

  }
})
