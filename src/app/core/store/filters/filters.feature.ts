// core/store/products/filters.feature.ts
import { createFeature, createReducer, createSelector, on } from '@ngrx/store';
import { selectList } from '../products/products.feature';
import { FiltersActions } from './filters.actions';

export interface FiltersState {
  text: string;
  gender: 'M' | 'F';
  pippo: boolean
}

export const initialState: FiltersState = {
  text: '',
  gender: 'F',
  pippo: true
}

export const filtersFeature = createFeature({
  name: 'filters',
  reducer: createReducer(
    initialState,
    on(FiltersActions.search, (state, action) => {
      const { type, ...rest } = action;
      return ({ ...state, ...rest})
    }),
  ),
});

export const {
  name, // feature name
  reducer, // feature reducer
  selectText, // selector for `text` property
  selectFiltersState,
  selectGender
} = filtersFeature;


// custom selector
export const selectFilteredList = createSelector(
  selectList,
  selectText,
  (list, text) => list.filter(item => item.name.toLowerCase().includes(text.toLowerCase()))
)

