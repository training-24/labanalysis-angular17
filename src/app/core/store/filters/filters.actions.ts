import { createActionGroup, props } from '@ngrx/store';
import { FiltersState } from './filters.feature';

export const FiltersActions = createActionGroup({
  source: 'Filters API',
  events: {
    // 'Search': props<{ text: string}>(),
    'Search': props<Partial<FiltersState>>(),
  }
});
