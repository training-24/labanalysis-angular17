import { HttpClient } from '@angular/common/http';
import { inject } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { catchError, interval, map, mergeMap, of, withLatestFrom } from 'rxjs';
import { Product } from '../../../model/product';
import { selectValue } from '../counter/counter.feature';
import { ProductsActions } from './products.actions';


export const loadProducts = createEffect(
  (
    actions = inject(Actions),
    http = inject(HttpClient),
    store = inject(Store),
  ) => {
    // ...
    return actions
      .pipe(
        ofType(ProductsActions.load),
        mergeMap(
          () => http.get<Product[]>('http://localhost:3000/products/')
            .pipe(
              map((items) => ProductsActions.loadSuccess({ items })),
              catchError(() => of(ProductsActions.loadFail()))
            )
        ),

      )
  },
  { functional: true }
)
/*

export const loadProducts2 = createEffect(
  (
    actions = inject(Actions),
    http = inject(HttpClient),
    store = inject(Store),
  ) => {
    // ...
    return actions
      .pipe(
        ofType(ProductsActions.load),
        concatLatestFrom(() => store.select(selectValue)),
        mergeMap(
          ([action, counter]) => http.get<Product[]>('http://localhost:3000/products/' + counter)
            .pipe(
              map((items) => ProductsActions.loadSuccess({ items })),
              catchError(() => of(ProductsActions.loadFail()))
            )
        ),

      )
  },
  { functional: true }
)
*/
