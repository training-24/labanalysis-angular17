import { computed, Injectable, signal } from '@angular/core';

interface Auth {
  token: string;
  role: 'admin' | 'mod';
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  auth = signal<Auth | null>(null)

  isLogged = computed(() => {
    return !!this.auth()
  })

  role = computed(() => {
    return this.auth()?.role
  })

  constructor() { }

  login() {
    // http...
    const res: Auth = { token: '...', role: 'admin'}
    this.auth.set(res);
  }

  logout() {
    // http...
    this.auth.set(null);
  }

}
