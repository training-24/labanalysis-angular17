import { provideHttpClient } from '@angular/common/http';
import { ApplicationConfig, isDevMode } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideState, provideStore } from '@ngrx/store';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { provideEffects } from '@ngrx/effects';
import { cartFeature } from './core/store/cart/cart.feature';
import { filtersFeature } from './core/store/filters/filters.feature';
import * as ProductsEffects from './core/store/products/products.effects';
import { productsFeature } from './core/store/products/products.feature';


export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideHttpClient(),
    provideStore(),
    provideState({ name: productsFeature.name, reducer: productsFeature.reducer }),
    provideState({ name: cartFeature.name, reducer: cartFeature.reducer }),
    provideState({ name: filtersFeature.name, reducer: filtersFeature.reducer }),
    provideStoreDevtools({ maxAge: 25, logOnly: !isDevMode() }),
    provideEffects([ProductsEffects])
  ],

};
